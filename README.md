# swiftBluetoothChat
💬 A example of chat written in Swift with CoreBluetooth, with readable code and very well documented.

# TODO

- [x] list of nearby bluetooths
- [ ] list **only** chat rooms (and not all bluetooths devices)
- [x] creat a new chat room
- [ ] chat room with name, and modifiable
- [x] send and read a message
- [ ] users with name
- [ ] send photo to chat
